# State of the Art

Research networks of two countries are presented below. 
It cannot be ruled out that further countries will be analyzed as the project progresses.

## Research Network Australia

Australia has already established a research data portal: the Population Health Research Network (PHRN) (https://www.phrn.org.au/). 
It brings together routine data from all government agencies and allows scientists to access and analyze pseudonymized data (https://doi.org/10.1515/itit-2019-0024). 
Data sets from multiple regional nodes in different jurisdictions can be accessed, like birth and death registrations, cancer registries, admissions to public and private hospitals or mental health data. 
Requests to this portal must specify the data sets and variables that are needed to answer the research question. 
This is currently done by manually filling out an Excel sheet per data set and jurisdiction and justifying each requested variable. 
Depending on the sources of the data, the delivered data sets might consist of data in various formats split into several files, which can make them quite hard to process. 
Access to this data is only granted for a limited time, after which the data must be disposed.

One option for data retrieval is the SURE platform (https://www.saxinstitute.org.au/solutions/sure/), which also links other data sources. 
SURE can be used to analyze the requested data sets in a secure environment. 
It uses a four-eye-principle for data to enter or exit a research context, keeping track of ethics approvals. 
At the end of the retention period, data is automatically encrypted and archived.


## National Research Data Infrastructure / Nationale Forschungsdateninfrastruktur (NFDI) in Germany

from https://forschungsdaten.info/fdm-im-deutschsprachigen-raum/deutschland/nfdi-nationale-forschungsdateninfrastruktur/informationen-zur-nfdi-nationale-forschungsdateninfrastruktur/, 
from https://forschungsdaten.info/praxis-kompakt/glossar/
and from https://forschungsdaten.info/themen/beschreiben-und-dokumentieren/metadaten-und-metadatenstandards/

The National Research Data Infrastructure (NFDI) has been under construction since 2019 to organize data infrastructures and research data management in Germany in a cooperative and efficient manner. 
In addition to subject-specific services, the NFDI is to establish training offers for researchers and set standards for the handling of research data.

In the so-called consortia, service portfolios are therefore developed that can be assigned to a subarea of the scientific system in terms of subject, topic and method. 
Within the individual consortia, a number of universities, research centers and other research institutions are usually involved. 
The NFDI consortia were selected through a process led by the German Research Foundation (DFG) in three funding rounds. 
In October 2020, the nine consortia of the first round started their work, and ten additional consortia have been funded since October 2021 (overview). 
The consortia of the third funding round will start in March 2023.

The publication of research data in a way that can be found and traced decisively promotes the quality of the data set and the potential for subsequent use. 
Data is usually not self-explanatory, but requires additional information called metadata. 
Well-designed and documented metadata therefore play a central role in finding, understanding and re-using research data.

The more structured the information is, the easier it is to read and process, not only for humans but also for machines. 
Standardization of the fields and values used helps to relate different data sets to each other and to make them findable and understandable across institutional, linguistic, and disciplinary boundaries.

Metadata in the research context contain structured information about research results, for example data sets or even code. 
They are stored or linked together with the descriptive data.

Different types of metadata fulfill different functions:

Bibliographic metadata such as title, authors, description, or keywords enable citation of data and code and help with findability and thematic containment.
Administrative metadata on file types, locations, access rights, and licenses help with the management and long-term preservation of data.
Process metadata describes the steps and actions, with their methods and tools used, that were applied to create and process the data.
Content descriptive or descriptive metadata can vary greatly in structure depending on the discipline and provide additional information about the content and origins of the data.
While bibliographic and administrative metadata can be standardized across disciplines, metadata on the process and content of research results often have a very discipline-specific structure and content. 
It is this subject-specific information that is often crucial for the discoverability and traceability of research data. 
Accordingly, there are many different metadata standards that provide a structure for the relevant information in a field or discipline.

A widely used standard for the bibliographic description of research data is the metadata schema for registering DOIs (digital object identifiers) from DataCite. 
This specifies which information about a dataset is mandatory (e.g. author, title), which information is recommended (e.g. subject area, description) and which is optional (e.g. funding, usage rights). These and other metadata are provided in XML format for interoperable use.

One standard for administrative metadata in long-term archiving is PREMIS. 
This standard can be used to describe objects in relation to actors, events and rights.

METS (Metadata Encoding & Transmission Standard), on the other hand, is an example of a container standard that specifies a structure of seven sections (header, indexing information, administrative information, file section, structure description, structure linkage, and behavior), for the content of which different metadata standards can then be selected in each case.

A large number of standards exist for subject-specific metadata. An overview of existing standards can be found in the Metadata Standards Catalog of the RDA and the page of the RDA Metadata Standards Directory Working Group, FairSharing.org or DDC (Digital Curation Centre).

For the medical and healthcare sectors, the various sites mention FHIR (Fast Healthcare Interoperability Resources), OMOP CDM (The Observational Medical Outcomes Partnership Common Data Model), and ODM (Operational Data Model), among others.

While XML-based metadata schemas provide a structure, i.e. define which information must, should and can be specified in which format, vocabularies and terminologies assist in standardizing the content. 
These range from controlled word lists, which standardize incorrect or different spellings of concepts, to taxonomies and thesauri, which contain super- and sub-terms as well as synonyms for concepts, to ontologies, which model properties and relations between concepts. 
An overview of existing terminologies is provided by the Basic Register of Thesauri, Ontologies and Classifications BARTOC. 
Terminology services allow - often subject-specific - searching for terminology terms.

For the medical and health sector, the following projects are to be highlighted in the BARTOC listing: 
* DIMDI publishes official medical classifications on behalf of the German Federal Ministry of Health and provides other terminologies and standards for the healthcare sector.
* The mission of Orphadata is to provide the scientific community with a comprehensive, high-quality and freely-accessible dataset related to rare diseases and orphan drugs, in a reusable format.
* eHealth Terminology Registries for Health care systems that may use different local names, codes, or wording for the same clinical concepts - for example, labs may have different names or numerical codes assigned to a lab test. When health care data from different sources is being shared, it is essential to be able to translate or resolve these differences, to ensure correct interpretation by both people and systems.