# Core Innovations

We recognize the variability of datasets used for different research projects in various fields of medical research and the need for data minimization due to strict privacy requirements. 
We want to evaluate the usage of existing query and/or mapping technologies like FHIRPath, CQL or FML to define granular access control patterns to the underlying EHR data on a very fine-grained level. 
The result of applying these access control patterns should be a restricted dataset to streamline further data processing pipelines. 
Our goal is to find a standardized solution which might be used during the approval process for an ethics committee and can be used to process the data automatically. 
It should also be possible to visualize and explain to patients their data donations to individual research projects. 
Additionally, the sharing and finding of research projects, using our standardized solution, should by made as easy as possible, similar to Open-Source platforms. 

An additional aim of our solution is to cope with the dynamic nature of EHR queries, that hinders the reproducibility of studies. 
Therefore, we want to explore the git technology to create reproduceable and verifiable datasets. 
Recognizing the maturity of the underlying technology and their integration into various existing software products, we hope to find a performant and ready-to-use solution for realistic scenarios with regards to data volume and update frequency.
We also hope to leverage existing tools in this ecosystem to solve issues, that might be discovered when defining such a solution.